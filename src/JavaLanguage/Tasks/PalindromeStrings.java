package JavaLanguage.Tasks;

import java.util.Scanner;

public class PalindromeStrings {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input;
        do {
            System.out.print("Your text:");
            input= scanner.nextLine().trim();
            System.out.printf("%s is%s a palindrome%n",input,testPalindrome(input)?"":" not");
            System.out.println("Do you want to test another string (Y/N)");
        }while(!scanner.nextLine().toLowerCase().startsWith("n"));

        scanner.close();
    }

    private static boolean testPalindrome(String input) {
        if(input == null)
            return  false;
        if(input.length()==1)
            return true;

        int mid = input.length()/2, last = input.length()-1 ;
        for(int i=0;i<mid;i++)
        {
            if(input.charAt(i) != input.charAt(last-i))
                return  false;
        }
        return true;
    }

}
