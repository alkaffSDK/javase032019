package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class EchoServerClass {

    public static void main(String[] args) {

        String line  = "";
        Socket client ;
        ArrayList<ClientReader> clientReaders = new ArrayList<>() ;
        ArrayList<ClientsWriter> clientsWriters = new ArrayList<>() ;
        MessageBoardCaster mMessageBoardCaster = null;

        ArrayList<Socket> clients = new ArrayList<>();
        try(ServerSocket serverSocket = new ServerSocket(Constants.SERVER_PORT))
        {
            System.out.println("Server started and waiting for client to connected..");
            do {
                client =  serverSocket.accept() ;
                System.out.println("Client is connected from "+client.getInetAddress());
                clients.add(client);
                clientReaders.add(new ClientReader("Client"+client.getInetAddress(),client) )  ;
                clientsWriters.add(new ClientsWriter(client)) ;
                if(mMessageBoardCaster == null)
                    mMessageBoardCaster = new MessageBoardCaster(clientsWriters);

            }while (true);


        } catch (IOException e) {
            e.printStackTrace();
        }finally {

        }
        System.out.println("Server is closed");
    }
}
