package JavaLanguage;



import java.text.NumberFormat;
import java.util.Locale;

public class Operators {
    /**
     * + , - ,*, /, % , < , >, <=, >= , =, == , != , ++ , -- , (-) , +=, -= , &&, ||,
     *  & , | , << , >> , >>> , ~ ,
     */
    public static void main(String[] args) {

        int a = 10, b = 3 ;

        System.out.println(a%b);


        System.out.println(a++);        // 10 then add 1 to a to be 11
        System.out.println(a);          // 11
        System.out.println(++a);         // add 1 to a (was 11) to be 12 then print the new value 12


        // a  = 12, b = 3
        System.out.println(a == b);     // means is a equals to b (true/false)
        System.out.println(a != b);
        System.out.println(a > b);
        System.out.println(a < b);
        System.out.println(a <= b);
        System.out.println(a >= b);

       // System.out.println(a && b );        // Error: because && accepts boolean operand only


        System.out.println(a >10 && b > 10);    //

        int r = a ;
        r *= b + 2 ;        // r = r * ( b +2 );
        System.out.println(r);

        a = 2;
        b = 3 ;

        int c = 3* a - b * 2 * (a + b) / 3;     // -10 , -4 , 0
        // c = -4;

        System.out.println("First c :"+c);

        // a = 4  , b=5
        c = 3* a++ - b++ * 2 * (a++ + b++) / 3;   // -40 , -4 , -4
        //   c = -8 ;
        System.out.println("Second c :"+ c);


        //TODO: Operators  27/05/2018

        a = -10;         //  00000000001010
        b = 3 ;         //

        Locale locale = Locale.getDefault() ;
        System.out.printf(locale,"%-10s :%11d -->%s%n","a",a,String.format(locale,"%32s",Integer.toBinaryString(a)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","b",b,String.format(locale,"%32s",Integer.toBinaryString(b)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a & b",a & b,String.format(locale,"%32s",Integer.toBinaryString(a & b)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a | b",a | b,String.format(locale,"%32s",Integer.toBinaryString(a | b)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a ^ b",a ^ b,String.format(locale,"%32s",Integer.toBinaryString(a ^ b)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","~a",~a,String.format(locale,"%32s",Integer.toBinaryString(~a)).replaceAll(" ","0"));

        System.out.println("-----------   <<   ---------------");
        System.out.printf(locale,"%-10s :%11d -->%s%n","a << 0",a << 0,String.format(locale,"%32s",Integer.toBinaryString(a << 0)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a << 1",a << 1,String.format(locale,"%32s",Integer.toBinaryString(a << 1)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a << 2",a << 2,String.format(locale,"%32s",Integer.toBinaryString(a << 2)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a << 3",a << 3,String.format(locale,"%32s",Integer.toBinaryString(a << 3)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a << 4",a << 4,String.format(locale,"%32s",Integer.toBinaryString(a << 4)).replaceAll(" ","0"));


        System.out.println("-----------   >>   ---------------");
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >> 0",a >> 0,String.format(locale,"%32s",Integer.toBinaryString(a >> 0)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >> 1",a >> 1,String.format(locale,"%32s",Integer.toBinaryString(a >> 1)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >> 2",a >> 2,String.format(locale,"%32s",Integer.toBinaryString(a >> 2)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >> 3",a >> 3,String.format(locale,"%32s",Integer.toBinaryString(a >> 3)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >> 4",a >> 4,String.format(locale,"%32s",Integer.toBinaryString(a >> 4)).replaceAll(" ","0"));


        System.out.println("-----------   >>>   ---------------");
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >>> 0",a >>> 0,String.format(locale,"%32s",Integer.toBinaryString(a >>> 0)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >>> 1",a >>> 1,String.format(locale,"%32s",Integer.toBinaryString(a >>> 1)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >>> 2",a >>> 2,String.format(locale,"%32s",Integer.toBinaryString(a >>> 2)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >>> 3",a >>> 3,String.format(locale,"%32s",Integer.toBinaryString(a >>> 3)).replaceAll(" ","0"));
        System.out.printf(locale,"%-10s :%11d -->%s%n","a >>> 4",a >>> 4,String.format(locale,"%32s",Integer.toBinaryString(a >>> 4)).replaceAll(" ","0"));


         a = 10 ;
        System.out.println(a < 0 && a++ != 10);         // false
        System.out.println(a);                          // 10       , because with logical operator (&&,||) it uses the short circuit 
        System.out.println(a < 0 & a++ != 10);          // false
        System.out.println(a);                          // 11
    }
}
