package exceptions;

import java.io.*;
import java.util.Locale;
import java.util.Scanner;

public class ExceptionWithFinally {

    public static void readFromFile(String filepath) {

//        System.out.println(10/0);

        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(filepath));             //2
            while (scanner.hasNextLine()) {                         //1
                System.out.println(scanner.nextLine());             //2
            }


            // scanner.close();
            // Exception happened  here

        } catch (FileNotFoundException e) {
            System.err.printf(Locale.getDefault(), "File %s is not exist. %n", filepath);
        } catch (IllegalStateException ex) {
            System.err.println("Can't open/read from the file:" + filepath);
        } catch (NullPointerException ex) {
            System.err.println("File path is null.");
        } finally {
            try {
                if (scanner != null) scanner.close();
            } catch (IllegalStateException ex) {
                System.err.println(ex.getMessage());
            }

        }
//
//        if(scanner != null)
//        scanner.close();

    }

    public static void readFromFile1(String filepath) {
        // try with resource , since Java 7
        try (Scanner scanner = new Scanner(new File(filepath)) )
        {
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }

            // Exception happened  here

        } catch (FileNotFoundException | IllegalStateException e) {
            e.printStackTrace();
        }
    }

    // try with multiple resources
    public void CopyFile(String source, String destination) {
        try (BufferedReader reader = new BufferedReader(new FileReader(source));
             PrintWriter writer = new PrintWriter(new FileWriter(destination))) {

            // TODO : copy the file content, this will be done on FileIO API later

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {


        String file = null; //  "D:\\target3.txt";
        readFromFile(file);
    }
}
