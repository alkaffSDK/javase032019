package JavaLanguage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class InputData {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner scanner = new Scanner(System.in);

        System.out.print("A :");
        int a =scanner.nextInt();
        System.out.println("You entered :"+a);
        System.out.print("What is your name:");
        scanner.nextLine();
        String name = scanner.next();
        System.out.println("Welcome :"+name);
        System.out.println(scanner.next());

        scanner.close();
    }
}
