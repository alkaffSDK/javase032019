package JavaLanguage;



import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class ArraysDemo {

    // What is the arrays : set of variables of the same type that are after each others in the memory
    public static void main(String[] args) {

        // this will define a variable of type array

        Scanner scanner = new Scanner(System.in);
        int[] marks = {1, 5, 9};                          //  {1,5,9}
        int[] marks1 = new int[20];                      // {0,0,0,0}
        int[] marks2 = new int[]{0, 2, 4, 6, 8, 10};         // {0,2,4,6,8,10}

        System.out.println(marks[0] + "\t" + marks[1] + "\t" + marks[2]);

        System.out.println(marks1[0] + "\t" + marks1[1] + "\t" + marks1[2] + "\t" + marks1[3]);

        System.out.println(marks2[0] + "\t" + marks2[1] + "\t" + marks2[2] + "\t" + marks2[3] + "\t" + marks2[4] + "\t" + marks2[5]);


        // Fill the array
//        System.out.println("Please fill the marks");
//        for(int i=0;i<marks1.length;i++)
//        {
//            System.out.print("Mark number ["+i+"] :");
//            marks1[i] = scanner.nextInt();
//        }


        // Fill array randomly

        Random random = new Random();
        System.out.print("Size:");
        int size = scanner.nextInt();

        int min = 35, max = 100 ;
        System.out.print("Min value :");
        min = scanner.nextInt();
        System.out.print("Max value :");
        max = scanner.nextInt();



        double maxvalue = Double.MIN_VALUE , minvalue = Double.MAX_VALUE;
        double[] grades= new double[size];
//        for(int i=0;i<grades.length;i++)
//        {
//            grades[i] = min + random.nextDouble() * (Math.abs(max-min)+1);
//            grades[i] = grades[i] > max ? max :grades[i] ;
//
//        }

        System.out.print("{");
        for (int index = 0; index < grades.length; index++) {
            grades[index] = min + random.nextDouble() * (Math.abs(max-min)+1);
            grades[index] = grades[index] > max ? max :grades[index] ;
            if(index > 0 && (index % 20) == 0)
                System.out.println();

            System.out.printf(Locale.getDefault(), "%6.2f,", grades[index]);



            if(grades[index] > maxvalue)
                maxvalue = grades[index];
            if(grades[index] < minvalue)
                minvalue = grades[index];
        }
        System.out.print("\b}\n");

        System.out.printf(Locale.getDefault(),"The maximum is :%6.2f%n",maxvalue);
        System.out.printf(Locale.getDefault(),"The minimum is :%6.2f%n",minvalue);


        printArray(marks);
        printArray(marks2);


//


        scanner.close();
    }

    private  static void printArray(int [] arr)
    {
        System.out.print("{");
        for (int index = 0; index < arr.length; index++)
            System.out.print(arr[index] + ",");
        System.out.print("\b}\n");
    }
}
