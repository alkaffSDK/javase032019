package oop;


import oop.sharedClasses.User;

public class EncapsulationDemo {
    public static void main(String[] args) {


        User user = new User();
//        user.Public = 10 ;      // I can see public from anywhere

        // can't access protected, private or default from another package
//        user.Protected = 1 ;
//        user.Private = 2;
//        user.Default = 3;



    }
}
