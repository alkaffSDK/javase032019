package JavaLanguage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;

public class StringsDemo {

    public static void main(String[] args) throws FileNotFoundException {

        String str = "String";
        String str1 =  "String";

        String string = new String("String");
        String string1 = new String("String");

        System.out.printf("%10s :%s ->%s%n","str",str,Integer.toHexString(str.hashCode()));
        System.out.printf("%10s :%s ->%s%n","str1",str1,Integer.toHexString(str1.hashCode()));
        System.out.printf("%10s :%s ->%s%n","string",string,Integer.toHexString(string.hashCode()));
        System.out.printf("%10s :%s ->%s%n","string1",string1,Integer.toHexString(string1.hashCode()));

        System.out.printf("%8s == %-8s :%s%n","str", "str1",(str == str1));
        System.out.printf("%8s == %-8s :%s%n","str","string",(str == string));
        System.out.printf("%8s == %-8s :%s%n","string", "string1",(string == string1));

        System.out.printf("%-25s :%s%n","str.equals(str1)",(str.equals(str1)));
        System.out.printf("%-25s :%s%n","str.equals(string)",(str.equals(string)));
        System.out.printf("%-25s :%s%n","string.equals(string1)",(string.equals(string1)));

        str = "string";
        System.out.println("-------------------------------");

        System.out.printf("%10s :%s ->%s%n","str",str,Integer.toHexString(str.hashCode()));
        System.out.printf("%10s :%s ->%s%n","str1",str1,Integer.toHexString(str1.hashCode()));
        System.out.printf("%10s :%s ->%s%n","string",string,Integer.toHexString(string.hashCode()));
        System.out.printf("%10s :%s ->%s%n","string1",string1,Integer.toHexString(string1.hashCode()));

        System.out.printf("%8s == %-8s :%s%n","str", "str1",(str == str1));
        System.out.printf("%8s == %-8s :%s%n","str","string",(str == string));
        System.out.printf("%8s == %-8s :%s%n","string", "string1",(string == string1));

        System.out.printf("%-25s :%s%n","str.equals(str1)",(str.equals(str1)));
        System.out.printf("%-25s :%s%n","str.equals(string)",(str.equals(string)));
        System.out.printf("%-25s :%s%n","string.equals(string1)",(string.equals(string1)));

        System.out.printf("%-35s :%s%n","str.equalsIgnoreCase(str1)",(str.equalsIgnoreCase(str1)));
        System.out.printf("%-35s :%s%n","str.equalsIgnoreCase(string)",(str.equalsIgnoreCase(string)));
        System.out.printf("%-35s :%s%n","string.equalsIgnoreCase(string1)",(string.equalsIgnoreCase(string1)));


        str = "   this    is the test string to    use    ";

        System.out.println(str.length());
        String replaced = str.replaceAll("s","m");
        System.out.println(replaced);
        System.out.println(str);

        System.out.println(str.replace("is","M"));
        System.out.println("string".compareTo("string"));
        System.out.println("string".compareTo("String"));
        System.out.println("string".compareTo("stringm"));
        System.out.println("string".compareTo(" string"));
        System.out.println("string".compareTo("strin"));

        System.out.println(str.trim());
        System.out.println(str);

        System.out.println(str.concat(str1));
        System.out.println(str+ str1);

        System.out.println(str.charAt(5));

        for(int i=0;i<str1.length();i++)
            System.out.println(str1.charAt(i));

        System.out.println(str.indexOf(" is"));

       System.out.println(str.split(" "));

        String[] splited = str.split("is");

        for(int i=0;i<splited.length;i++)
            System.out.println(splited[i]);


        Scanner scanner = new Scanner(new File("D:\\Java Workspace\\Java032018\\test.txt")) ;

        String line = "";
        String[] spilt ;
        while (scanner.hasNextLine())
        {
            line = scanner.nextLine();
            spilt = line.replaceAll("\"","").split(",");
            for(int i=0;i<spilt.length;i++)
            {
                System.out.print(spilt[i]+ "\t");
            }
            System.out.println();
        }

        scanner.close();
    }
}
