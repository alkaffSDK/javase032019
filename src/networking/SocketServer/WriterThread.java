package networking.SocketServer;

import java.io.*;
import java.util.Scanner;

public class WriterThread extends  Thread {
    private  OutputStream  outputStream ;
    public WriterThread(OutputStream  outputStream) {
        this.outputStream = outputStream ;
    }

    @Override
    public void run() {
        try {
            PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
            Scanner sc = new Scanner(System.in);
            String line = null ;
            while (true)
            {
                // System.out.print("You :");

                line = sc.nextLine() ;
                printWriter.println(line);
                printWriter.flush();
                if(line.equalsIgnoreCase("bye"))
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

