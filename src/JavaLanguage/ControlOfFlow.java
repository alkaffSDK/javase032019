package JavaLanguage;

import java.util.Scanner;

public class ControlOfFlow {

    /**
     * Control of flow statements
     * <p>
     * 1) if statements
     * <p>
     * Syntax :
     * if(<boolean_expression>)
     * statement ; or block {}
     * [else statement ; or block {} ]
     * <p>
     * <p>
     * 2) switch statement
     * <p>
     * Syntax :
     * <p>
     * switch(<some linear variables >)
     * {
     * [
     * case <constant_unique_value> :
     * [statements]*
     * ]*
     * [
     * default:
     * [statements]*
     * ]
     * }
     * <p>
     * 3) conditional operator
     *          Syntax : <boolean_expression> ? <value_if_true> :<value_if_false>
     *
     */


    public static void main(String[] args) {
        int a = 3, b = 5;
        if (a == b) {
            ;
            System.out.println("a is equal to b ");
        } else {
            System.out.println("is is not equal to b");
            System.out.println("Done");
        }

        final int r = 2;

        switch (a) {
            case 0:
                System.out.println();
            case r - 1:
                System.out.println("a is equal to b ");
                break;
            default:
                System.out.println("is is not equal to b");
        }

        int d = 20;
        switch (d) {
            case 0:
                System.out.println("Zero");
                break;
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            default:
                System.out.println("Default");
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("mark:");
        int mark = scanner.nextInt();

        if (mark >= 35 && mark < 50) {
            System.out.println("F");
        } else {
            if (mark >= 50 && mark < 68) {
                System.out.println("P");
            } else {
                if (mark >= 68 && mark < 76) {
                    System.out.println("G");
                } else {
                    if (mark >= 76 && mark < 84) {
                        System.out.println("V.G");
                    } else {
                        if (mark >= 84 && mark <= 100) {
                            System.out.println("E");
                        } else {
                            System.out.println("Error");
                        }
                    }
                }
            }
        }

        // another way : simpler
        if (mark < 35 || mark > 100) {
            System.out.println("Error");
        } else {            // 35 - 100
            if (mark < 50 ) {
                System.out.println("F");
            } else {        // 50 - 100
                if (mark < 68) {
                    System.out.println("P");
                } else {        // 68-100
                    if (mark < 76 ) {
                        System.out.println("G");
                    } else {            // 76 - 100
                        if (mark < 84 ) {
                            System.out.println("V.G");
                        } else {        // 84- 100
                            System.out.println("E");
                        }
                    }
                }
            }
        }

        System.out.println((mark < 35 || mark > 100)?"Error": (mark < 50 ? "F" : mark < 68 ? "P" : mark < 76 ? "G" : mark < 84 ? "V.G":"E") );



        int number = scanner.nextInt();

        if((number & 1)==0)
            System.out.println("Even");
        else
            System.out.println("Odd");

        int abs = 0 ;
       if(number >= 0)

           System.out.println(number);
       else
           System.out.println(-number);


       abs = number >= 0 ? number : -number ;

        System.out.println(number >=0 ? number : -number);

        // number >= 0 ? number : -number ;
        // number ;

        scanner.close();
    }
}
