package networking;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class NetworkingDemo {

    public static int PORT = 12545 ;
    public static String IP_ADDRESS = "127.0.0.1";  //localhost

    static Scanner scanner = new Scanner(System.in);
    static class Server extends Thread{
        @Override
        public void run() {

            try {
                ServerSocket server = new ServerSocket(PORT);
                Socket client = server.accept();
                System.out.println("A client has been connected with IP:"+client.getInetAddress());

                new Thread(() -> {
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream())) )
                    {
                        String line = "";
                        while ((line = reader.readLine()) != null){
                            System.out.println("Client said :"+line);
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }).start();

                try (PrintWriter pw = new PrintWriter(new BufferedOutputStream(client.getOutputStream()))  )
                {
                    pw.println("Welcome to my network");
                    pw.flush();
                    do {
                        System.out.print("Server :");
                        pw.println(scanner.nextLine());
                        pw.flush();
                    }while (true) ;


                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }

            } catch (IOException e) {
                System.err.println("Socket could not be opened !");
            }
        }
    }

    static class Client extends Thread {
        @Override
        public void run() {
            try {
                Socket socket = new Socket(IP_ADDRESS,PORT);
                new  Thread (()-> {
                    try (PrintWriter pw = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()))  )
                    {
                        do {
                            System.out.print("Client :");
                            pw.println(scanner.nextLine());
                            pw.flush();
                        }while (true) ;


                    }catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }).start();
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream())) )
                {
                    String line = "";
                    while ((line = reader.readLine()) != null){
                        System.out.println("Server said :"+line);
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                System.err.println("Socket could not be opened !");
            }


        }
    }

    public static void main(String[] args) {

        Server server = new Server();
        server.start();



    }

}
