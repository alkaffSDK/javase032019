package tasks;

import lamdaExpression.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Task2 {
    public static void main(String[] args) {
        // 1: Create list of person in the class
        List<Person> people = Arrays.asList(
                new Person("Ahmed", "Alkaff", 34),
                new Person("Raghad", "Mando", 22),
                new Person("Omama", "Manaa", 26),
                new Person("Yazan", "AbuHamdodeh", 26),
                new Person("Imaseil", "Bibers", 23),
                new Person("Ahmed", "Dosogqei", 25),
                new Person("Yaser", "Meryan", 23)

        );

        //TODO: 2: Sort list by last name


        //TODO: 3: Create a method that prints all elements in the list

        // TODO: 4: Create a method that prints all people that have last name beginning with M

    }
}
