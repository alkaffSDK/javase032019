package oop;

import oop.sharedClasses.User;

public class SettersAndGettersDemo {
    public static void main(String[] args) {

        User u = new User();
        //u.Name = "Ahmed";
        u.setName("Ahmed");
        //u.Age = 360 ;
        u.setAge(36);

        // u.Age = 360 ;
//        u.Login = "Alkaff";
//        u.password = "123";

        System.out.println("The age is :" + u.getAge());
        System.out.println("u = " + u);
    }
}
