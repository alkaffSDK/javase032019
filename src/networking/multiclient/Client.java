package networking.multiclient;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

	Scanner scaner = new Scanner(System.in);
	ClientConnection cc = null;
	Socket server;
	private boolean Running = true;

	String mSeverAddress = "127.0.0.1";

	public Client()
	{
		this("127.0.0.1");
	}

	public Client(String serverAddress) {
		mSeverAddress = serverAddress ;
		try {
			server = new Socket(mSeverAddress, Server.PORT);
			cc = new ClientConnection("Client", server);
			cc.start();
			while (Running ) {

				while (!scaner.hasNextLine())
				{
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				String input = scaner.nextLine();

				if (input.toLowerCase().equals("quit"))
					break;

				cc.SentMessageToServer(input);
			}
			
			cc.close();
			close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void close() {
		if (scaner != null)
			scaner.close();

		if (server != null)
			try {
				server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

	}

	public static void main(String[] args) {

		new Client();
	}

}
