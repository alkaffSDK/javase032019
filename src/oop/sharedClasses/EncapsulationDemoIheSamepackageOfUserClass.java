package oop.sharedClasses;

import oop.sharedClasses.Parent;

public class EncapsulationDemoIheSamepackageOfUserClass {
    public static void main(String[] args) {


        Parent user = new Parent();
        user.Public = 10;      // I can see public from anywhere

        user.Default = 1;
        user.Protected = 2;

        // can't access private  from the same package
        // user.Private = 3 ;


    }
}
