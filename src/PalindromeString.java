import java.util.Scanner;

public class PalindromeString {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input, lower;
        int last, i, mid;
        char ch;
        boolean isPalindrome = true;
        do {
            System.out.print("Enter a string to test:");
            input = scanner.nextLine();
            lower = input.toLowerCase().trim();

            for (i = 0, last = lower.length() - 1, mid = lower.length() / 2, isPalindrome = true; i < mid; i++, last--) {
                if (lower.charAt(i) != lower.charAt(last)) {
                    isPalindrome = false;
                    break;
                }
            }
            System.out.printf("The string \"%s\" is %s palindrome %n", input, isPalindrome ? "" : "not");
            System.out.println("Do you want to test another string (Y/N)");
            ch = scanner.nextLine().toLowerCase().charAt(0);
        } while (ch == 'y');

        System.out.println("Thanks you !");
        scanner.close();
    }
}
