package tasks;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskNumbers {

    // TODO: 1) read the data.txt files

    // TODO: 2) Find the frequency for each number in the file

    // TODO: 3) Find the missing number(s)

    // TODO: 4) Find the most frequent number(s)

    public static void main(String[] args) {

        String path = "D:\\Java Workspace\\javase032019\\src\\tasks\\data.txt";
        String line = "";
        String[] split;
        int number;

        int max = Integer.MIN_VALUE;
        HashMap<Integer, Integer> map = new HashMap<>();
        HashMap<Integer, List<Integer>> fre = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while ((line = reader.readLine()) != null) {
                split = line.split(",");
                for (String s : split) {
                    if (!s.trim().isEmpty() && !s.trim().isBlank()) {
                        number = Integer.valueOf(s.trim());
                        if (number > max)
                            max = number;
                        if (map.containsKey(number))
                            map.put(number, map.get(number) + 1);
                        else
                            map.put(number, 1);
                    }

                }
            }

            System.out.println("map = " + map);
            System.out.println("max = " + max);

            int key;
            for (int i = 0; i <= max; i++) {
                if (!map.containsKey(i))
                    System.out.println("i = " + i + " is missing.");
                else {
                    key = map.get(i);
                    if (!fre.containsKey(key))
                        fre.put(key, new ArrayList<>());
                    fre.get(key).add(i);
                }
            }

            System.out.println("fre = " + fre);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
