package JavaLanguage;

public class Variables {

    /**
     * Variables : is named reference for a memory location
     * Syntax to define a variable :
     * [access specifier] [modifier] DATA_TYPE <variable_id> [=<expression>] [,<variable_id> [=<expression>]] *;
     * <p>
     * DATA_TYPE:             size in byte              1       2     1      2     4    8        4      8
     *      1) Value types  (primitive types ): boolean, char ,[byte, short, int,long] , [float, double]
     *      2) Reference types (non-primitive types ): any class or interface or enum.
     * <p>
     * Note: any class, interface , enum is a data type !!
     * <p>
     * variable_id :
     * 1) not a keyword
     * 2) Unique in the same scope {block}
     * 3) Contains only [A-Z,a-z,0-9,_,$]
     * 4) Start with [A-Z,a-z,_,$]
     */

    public static void main(String[] args) {

        // boolean : true/false  (in Java 0 is not false and 1 is not a true)
        boolean bool1 = true;

        byte byte1 = -128;              // -128 to 127
        short short1 = -32768;          // -32768 to 32767
        int int1;
        long long1 = -9223372036854775808L;         // if constant is  larger than integer range then it must ends with L/l if bigger than int rand


        Hello hello ;
        int a = 10;
        System.out.println(a);          // 10       decimal
        a = 010;
        System.out.println(a);          // 8        octal -any number start with 0 is an octal
        a = 0x10;
        System.out.println(a);          // 16       Hexadecimal- any number start with 0x is an Hexadecimal
        a = 'A';
        System.out.println(a);          // 65       ASCII code for A which is 65
        a = '\u0041';
        System.out.println(a);          // 65       Unicode of 0041 which is 65 (A)
        // 101 , 05 , 0x5

        char char1 = ' ';
        System.out.println(char1);
        char1 = 8;
        System.out.println("hi" + char1);
        char1 = 0101;
        System.out.println(char1);
        char1 = 0x41;
        System.out.println(char1);
        char1 = '\u0041';
        System.out.println(char1);

        // in java : any floating point constant is considered to be double
        float float1 = 15.3f;           // float constant should end with f/F
        double double1 = 15.3;


        // Type convection  1       2      4        8       4        8
        // 1) Implicit : byte -> short -> int -> long -> float -> double   (char - > int / long)
        // Risky cases if the value is large
        // 1) int -> float
        int x = 2147483647;
        float f = x;
        System.out.println("X :" + x);
        System.out.println("F :" + f);
        // 2) long -> float
        long l = 9223372036854775807L;
        f = l;
        System.out.println("L :" + l);
        System.out.println("F :" + f);
        // 3) long -> double
        double d = l;
        System.out.println("L :" + l);
        System.out.println("D :" + d);
        short1 = byte1;

        //  2) Explicit

        x = (int) l;
        System.out.println("L :" + l);
        System.out.println("X :" + x);

        x = (int) 15.9;
        System.out.println("X :" + x);
    }
}
