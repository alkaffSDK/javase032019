package JavaLanguage;



import java.util.Locale;
import java.util.Scanner;

public class Loops {
    public static void main(String[] args) {

        /*

         */

//        byte a = 0 ;
//
//        while(a <= 0) {
//            ;
//            System.out.println(--a);
//        }
//        a = -10 ;
//        System.out.println("------------do while ----------------");
//        do
//            System.out.println(a--);
//        while (a > 0);
//
//        System.out.println("------------ while ----------------");
//        a = -10 ;
//        while(a > 0)
//            System.out.println(a--);
//
//        System.out.println("------------ for ----------------");
//        int j = 0 ;
//        for(int x = 0;x < 5; x++) {
//            j = x ;
//            while(j-- >0)
//                System.out.println("X:"+x+", J:"+j);

        int a = 1 ;
        do {
            System.out.println(a);
        }
        while(a <= 10);
//        }

        Locale locale = Locale.getDefault();
        Scanner scanner = new Scanner(System.in);

        System.out.flush();
        int r = 0 ;
        int n = scanner.nextInt(), s = scanner.nextInt() ;        // print all numbers from 1 to n that has the sum of s
        for(int x = 1;x <= n/2; x++) {
            r = s - x ;
           if(r > 0 && r <= n && x < r)
               System.out.printf("%3d + %3d = %3d%n",x,r,s);
        }

        scanner.close();



    }
}
