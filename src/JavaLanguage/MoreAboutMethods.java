package JavaLanguage;

import java.util.Locale;
import java.util.Random;

public class MoreAboutMethods {

    static class Some {
        int a ;
    }
    public static  void add5(int a)     //
    {
        a += 5 ;
    }

    public static  void add5(Some s)     //
    {
        s.a += 5 ;
    }

    public static  void add5(int[] arr)     //
    {
        for(int i=0;i<arr.length;i++)
            arr[i] += 5 ;
    }

    public static void PrintArray(int [] arr)
    {
        System.out.print("{");
        for(int i=0;i<arr.length;i++)
        {
            System.out.printf(Locale.getDefault(),"%5d,",arr[i]);
        }
        System.out.print("\b}\n");
    }
    public static void FillArray(int [] arr, int min, int max)
    {
        Random r = new Random();
        for(int i=0;i<arr.length;i++)
        {
           arr[i] = min + r.nextInt(Math.abs(max-min)+1);
        }
    }
    public static int findMin(int[] arr)
    {
        if(arr.length ==0)
            return Integer.MAX_VALUE;

        if(arr.length == 1)
            return  arr[0];

        int min = arr[0];
        for(int i=1;i<arr.length;i++)
        {
            if(arr[i] < min)
                min = arr[i];
        }

        return  min ;

    }
    public static int findMax(int[] arr)
    {
        if(arr.length ==0)
            return Integer.MIN_VALUE;

        if(arr.length == 1)
            return  arr[0];

        int max = arr[0];
        for(int i=1;i<arr.length;i++)
        {
            if(arr[i] > max)
                max = arr[i];
        }

        return  max ;

    }
    public static int[] findMinMax1(int[] arr)
    {
        int [] minmax = new int[2];

        if(arr.length ==0)
            return null;

        minmax[0] = arr[0];
        minmax[1] = arr[0] ;

        if(arr.length == 1)
        {
            return  minmax;
        }


        for(int i=1;i<arr.length;i++)
        {
            if(arr[i] > minmax[1])
                minmax[1]= arr[i];

            if(arr[i]  < minmax[0])
                minmax[0]= arr[i];
        }

        return  minmax ;


    }
    static  class MinMax {
        int min= Integer.MAX_VALUE, max = Integer.MIN_VALUE ;
    }
    public static MinMax findMinMax2(int[] arr)
    {
        MinMax mm = new MinMax();

        if(arr.length ==0)
            return null;

        if(arr.length == 1)
        {
            mm.min = arr[0];
            mm.max = arr[0];
            return  mm;
        }


        for(int i=0;i<arr.length;i++)
        {
            if(arr[i] > mm.max)
                mm.max= arr[i];

            if(arr[i]  < mm.min)
                mm.min= arr[i];
        }

        return   mm ;


    }
    public static void main(String[] args) {

        int x = 20 ;
        add5(x);
        System.out.println(x);

        Some r = new Some();
        r.a = 20 ;
        add5(r);
        System.out.println(r.a);

        int [] array ={2,6,8,2,1};
        add5(array);
        for(int i=0;i<array.length;i++)
            System.out.print(array[i]+",");


        System.out.println();
        int [] numbers = new int[30];

        FillArray(numbers,1,1000);
        PrintArray(numbers);
        int [] minmax = findMinMax1(numbers);
        MinMax result = findMinMax2(numbers);
        System.out.println("the max is :"+findMax(numbers)+", and min is :"+findMin(numbers));
        System.out.println("the max is :"+minmax[1]+", and min is :"+minmax[0]);
        System.out.println("the max is :"+result.max+", and min is :"+result.min);
    }
}
