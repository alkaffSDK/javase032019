package threading;

import java.util.Locale;

public class ThreadingDemo1 {

    // Thread :

    // Process:

    static class Tester {
        int x ;

        Boolean Lock = false ;

        public  void  increase(int counter)
        {
            System.out.printf("Thread %s is start working..%n",Thread.currentThread().getName());
            for (int i = 0; i < counter; i++) {
                synchronized (Lock) {
                    x++;
                }
                System.out.printf(Locale.getDefault(),"%-15s x:%d%n",Thread.currentThread().getName(),x);
            }
        }

        public  void decrease(int counter)
        {
            System.out.printf("Thread %s is start working..%n",Thread.currentThread().getName());
            for (int i = 0; i < counter; i++) {

                synchronized (Lock) {
                    x--;
                }
                System.out.printf(Locale.getDefault(), "%-15s x:%d%n",Thread.currentThread().getName(),x);
            }
        }
    }
    public static void main(String[] args) {
        int Counter = 10;
        System.out.println(Thread.currentThread().getName());


        WithThreading(Counter);
        System.out.println("==============");

      //  NoThreading(Counter);
    }

    public  static  void NoThreading(int c)
    {
        Tester tester = new Tester();
        long start = System.currentTimeMillis() ;
        tester.increase(c);
        tester.decrease(c);

        long end = System.currentTimeMillis() - start ;
        System.out.printf(Locale.getDefault(),"X values is :%-10d with Time:%20d(ms) %n",tester.x,end);
    }

    public  static  void WithThreading(int c)
    {
        Tester tester = new Tester();
        long start = System.currentTimeMillis() ;

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                tester.increase(c);
            }
        },"First");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                tester.decrease(c);
            }
        },"Second");

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis() - start ;
        System.out.printf(Locale.getDefault(),"X values is :%-10d with Time:%20d(ms) %n",tester.x,end);
    }
}
