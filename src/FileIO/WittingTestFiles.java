package FileIO;

import jdk.dynalink.StandardOperation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class WittingTestFiles {

    public static void main(String[] args) {
       // writeToFile("D:\\input.txt", "this is a new text",false);

        try {
            AtomicInteger i = new AtomicInteger(0) ;
            Files.lines(Paths.get("D:\\countries.txt")).forEach(l -> System.out.printf(Locale.getDefault(),"%5d) %s%n",i.incrementAndGet(),l));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private  static void writeToFile(String filepath, String text, boolean append)
    {
        try (FileOutputStream fos = new FileOutputStream(filepath,append)){

            fos.write(text.getBytes());
            //fos.flush();            // forse the output stream to write to the physical file

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private  static void writeToFile1(String filepath, String text, boolean append)
    {
        try (PrintWriter writer = new PrintWriter(new FileWriter(filepath,append))){

           writer.println(text);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private  static void writeToFile2(String filepath, String text, boolean append)
    {
        // Java 1.7 and above
        try {
            Files.writeString(Paths.get(filepath),text, StandardOpenOption.APPEND) ;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
