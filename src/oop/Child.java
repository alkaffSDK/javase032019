package oop;

import oop.sharedClasses.Parent;

public class Child extends  Parent{

    public  void method()
    {
        // from the child object you can access the protected members of the parent object only
        Protected = 10 ;


        Parent u = new Parent();
//        u.Default = 1 ;
        u.Public = 2 ;
        // you can't access the protected members of any other object, except the parent objects
      //  u.Protected = 3 ;
    }
    public static void main(String[] args) {

        Parent u = new Parent();
//        u.Default = 1 ;
        u.Public = 2 ;
        // you can't access the protected members of any object, except the parent objects
//        u.Protected = 3 ;
    }
}
