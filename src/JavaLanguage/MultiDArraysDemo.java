package JavaLanguage;

import java.util.Locale;
import java.util.Random;

public class MultiDArraysDemo {

    public static void main(String[] args) {
        int rows = 3, cols = 5 , depth = 2 ;
        int[][] matrix1 = new int[rows][cols];
        int[][] matrix2 = {{1},{2,3,4},{5,6,7,8,9,10}};
        int[][] matrix3 = new int[][] {{1,2,3},{4,5,6},{7,8,9}} ;


        Random rand = new Random();
        int[][][] ThreeD1 = new int[rows][cols][depth];


        printArray(matrix1);
        printArray(matrix2);
        printArray(matrix3);


        int init = 1 ;
        System.out.println("-------------------------");

        for(int r=0;r<ThreeD1.length;r++)
        {
            for(int c=0;c< ThreeD1[r].length;c++)
            {
                for(int d=0;d< ThreeD1[r][c].length;d++) {
                    ThreeD1[r][c][d] = init++;
                    System.out.print(ThreeD1[r][c][d] + ", ");
                }
                System.out.println();
            }
            System.out.println("++++++++++++++++++");

        }

        System.out.println("====================================");
        init = 1 ;
        int[][] randomMatrix = new int[1 + rand.nextInt(10)][];
        for(int i=0;i<randomMatrix.length;i++)
        {
            randomMatrix[i] = new int[1 + rand.nextInt(10)];
        }

        for(int r=0;r<randomMatrix.length;r++)
        {
            for(int c=0;c< randomMatrix[r].length;c++)
            {
                randomMatrix[r][c] =init++ ;
                System.out.printf(Locale.getDefault(),"%5d,",randomMatrix[r][c]);
            }
            System.out.println();
        }
    }

    private  static void printArray(int [][] arr)
    {
        for(int r=0;r<arr.length;r++)
        {
            for(int c=0;c< arr[r].length;c++)
            {
                System.out.print(arr[r][c]+ ", ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
