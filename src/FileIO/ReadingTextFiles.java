package FileIO;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

public class ReadingTextFiles {

    public static void main(String[] args) {
//        readingUsingScanner("D:\\text.txt");

        readingBufferReaderScanner("D:\\input.txt");

    }

    public static void readingUsingScanner(String filePath)
    {
        try (Scanner scanner = new Scanner(new File(filePath), Charset.forName("UTF-8")))
        {
            while(scanner.hasNextLine())
                System.out.println(scanner.nextLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingInputStreamScanner(String filePath)
    {
        try (InputStream is   = new FileInputStream(new File(filePath)))
        {
            byte[] bytes ;
            int size = 0 ;
            if (( size = is.available()) > 0 )
            {
                bytes = is.readAllBytes() ;
                System.out.println(new String(bytes));
              //  String.valueOf(bytes);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingBufferReaderScanner(String filePath)
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath))) )
        {
           String line = "";
           while ((line = reader.readLine()) != null)
           {
               System.out.println(line);
           }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
