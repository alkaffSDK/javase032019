package JavaLanguage;


public class ClassesAndMethods {
    /**
     * class : description/ template/ blueprint for a data type ; that defines its states (variables /objects )and behaviours (methods)
     *
     * method:  named block of code ({statements}) with return data type and optional inputs
     *
     *          all methods must be inside a class directly
     *
     *          Syntax : to define a method (function)
     *
     *                      RETURN_DATA_TYPE <method_name>([parameter_list]) { body}
     *
     */


    static  void print(int a)       // int a = 50
    {

        System.out.println(a);
        return;
        // System.out.println("");
    }

    static int sum(int a, int b )   // a 15, b 5
    {
        return a + b ;          // 15+ 5 = 20
       // System.out.println("Ater");
    }
    public static void main(String[] args) {

        Point p ;
        int x = 10 ;
        print(x);
        print(50);

        int s = sum(15,5);
        System.out.println("s = "+s);

        System.out.println(sum(55,85));
    }
    static class Point {
        // states
        int x , y ,z ;
        // behaviours
        public void changeX()
        {

        }
    }
}
