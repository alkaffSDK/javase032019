package oop;

import oop.sharedClasses.Point;
import oop.sharedClasses.Vehicle;

import java.util.Locale;
import java.util.Random;

public class InheritanceDemo {


    // In java : only single inheritance is allowed !!
    // There is only one and only one parent class for any class in Java (except for Object class)
    static class Parent extends Point {

        //

        // static member are no inheritable
        public static void staticMethod() {
            // Error  Protected = 10; //
        }

        // non static members are inheritable but not all of the are accessible  ( private and ( package for class in different packages )
        protected void nonStatic() {
            Public = 10;
            Protected = 10;
        }
    }

    static class Child extends Parent {

    }

    public static void main(String[] args) {

        Parent p = new Parent();
        Child c = new Child();
        Object d ;

    }
}
