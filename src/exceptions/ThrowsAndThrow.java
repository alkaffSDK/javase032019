package exceptions;

public class ThrowsAndThrow {


    static class Person {
        private int age;

        public int getAge() {
            return age;
        }

        /**
         *  Set the age value, age must be postive integer between 0 and 150
         * @param a the new age  value
         * @throws IllegalArgumentException if the age was outside the range 0- 150
         */

        public void setAge(int a) throws  IllegalArgumentException{
            if (a > 0 && a < 150)
                age = a;
            else
                throw  new IllegalArgumentException("invalid age value.");
        }

        public void setAge1(int a) throws Exception {
            if (a > 0 && a < 150)
                age = a;
            else
                throw  new Exception("invalid age value.");
        }

        @Override
        public String toString() {
            return "Person{" +
                    "age=" + age +
                    '}';
        }
    }

    public static void someMethod() {
        try {
            // TODO: do any thing that may cause an exception

            throw new ArithmeticException();
        } catch (NullPointerException | IllegalArgumentException e) {
            // Exception is checked exception so we have to do one of :
            // 1) surround it with try catch block
            // 2 ) annotate the method with throws
            //throw  new Exception();
        } catch (Exception ex) {
            // TODO: handle the caught exception
            // throw an Exception  variable does not force to deal with check exception
            // because we have no grantee about the type of exception object
            // if line 10 is   throw  new ArithmeticException(); then no check is needed
            // but if it was like   throw  new Exception() then check will be needed
            throw ex;
        } catch (Throwable t) {
            // TODO: log the exception then throw it back
            throw t;
        }
    }

    public static void main(String[] args) {


        Person p = new Person();
        try {
            p.setAge1(10);
            p.setAge(15);
            p.setAge(-10);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

        System.out.println(p);
    }
}
