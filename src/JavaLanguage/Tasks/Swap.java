package JavaLanguage.Tasks;



import java.util.Scanner;

public class Swap {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = 10, y = 50;

        x = scanner.nextInt();
        y = scanner.nextInt();
        System.out.println("X :" + x + ", Y:" + y);
        //TODO:1 swap the value of x and y

        int temp = x;
        x = y;
        y = temp;

        //TODO:2 swap the value of x and y with no extra space (find more than one solution)

        System.out.println("X :" + x + ", Y:" + y);

        scanner.close();
    }
}
